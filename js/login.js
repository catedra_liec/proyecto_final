//ACA BORRAMOS LA LLAME "SessionTemoral" para que cada vez que el usuario 
//cierre sesion se borre el correo de la llave po reso al final de este js la vuelco a crear 
//con otro correo que sera el verificado donde si exista ese usuario
localStorage.removeItem("SessionTemporal");

function iniciar() {

  var boton = document.getElementById("validar");
  if (boton.addEventListener) {
    boton.addEventListener("click", validar, false);
  } else if (boton.attachEvent) {
    boton.attachEvent("onclick", validar);
  }
}

function validar() {
  var txtCorreo = document.getElementById("txtCorreo").value;
  var txtContra = document.getElementById("txtContra").value;

  if (txtCorreo.length == 0 && txtContra.length == 0) {
    swal({
      title: "Error",
      text: "Campos vacíos",
      icon: "info",
      button: "Reintentar",
    });

    return 0;
  }
  if (txtContra == null || txtContra == "" || txtContra.length == 0 & txtCorreo.length > 0) {
    swal({
      title: "Error",
      text: "No se ha ingresado el campo de contraseña",
      icon: "error",
      button: "Reintentar",
    });
    return 0;
  }
  if (txtCorreo == null || txtCorreo == "" || txtCorreo.length == 0 & txtContra.length > 0) {
    swal({
      title: "Error",
      text: "No se ha ingresado el campo de usuario",
      icon: "error",
      button: "Reintentar",
    });

    return 0;
  }

  //Obtengo los datos de la llave "NumerosUsuarios"
  var NumerosUsuarios = JSON.parse(localStorage.getItem("NumerosUsuarios")) || [];
  //calculo la cantidad de datos que contiene en la variable "i"
  j = NumerosUsuarios.length;
  //empiezo un for para que un numero de 0 hasta el numero "i" se carguen y este 
  //lo obtengo para concatenar con los datos que se ingresen
  for (i = 0; i < j; i++) {
    var Correo = i + txtCorreo;
    var Contra = i + txtContra;

    //captura como ARREGLOS los datos de las llaves de "CorreosUsuarios" y la de "ContrasenasUsuarios"
    var CorreosLS = [] = JSON.parse(localStorage.getItem("CorreosUsuarios"));
    var ContrasenasLS = [] = JSON.parse(localStorage.getItem("ContrasenasUsuarios"));

    //JavaScript tiene la funcion de ".include" la cual busca en el array si la varible en parenteris existe dentro de este array
    //entonces del array de "CorreosUsuarios" y "ContrasenasUsuarios" busco los datos del login ya concatenados con el algun numero de "i"
    //si lo verifica porque en si el identifcador son los diferentes correos y contrasenas    
    if (CorreosLS.includes(Correo) == true) {
      if (ContrasenasLS.includes(Contra) == true) {
        //como el resultado de esta funcion solo es true o false, asi lo verifico
        swal({
          title: "Hola",
          text: "Bienvenido!",
          icon: "success",
          button: "Okay",
        });

        //Si todo coincide creo la llave "SessionTemporal" 
        var SessionTemporal = JSON.parse(localStorage.getItem("SessionTemporal")) || [];
        var CorreoShow = i + txtCorreo;
        SessionTemporal.push(CorreoShow);
        //en la llave "SessionTemporal" guardare el correo concatenado con su numero respectivo
        //debe ser de esta forma porque es directo ya que solo estamos guardando un dato
        localStorage.SessionTemporal = SessionTemporal;
        window.location = "html/usuario.html";

      } else {
        swal({
          title: "Error",
          text: "Contraseña incorrecta",
          icon: "error",
          button: "Reintentar",
        });
      }
    } else {
      swal({
        title: "Error",
        text: "Correo incorrecto",
        icon: "error",
        button: "Reintentar",
      });
    }
  }
}

//Asociando función que manejará el evento load al cargar la página
if (window.addEventListener) {
  window.addEventListener("load", iniciar, false);
} else if (window.attachEvent) {
  window.attachEvent("onload", iniciar);
}