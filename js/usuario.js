function iniciar(){
	var modal = document.getElementById("myModal");

	var btn = document.getElementById("myBtn");

	var span = document.getElementsByClassName("close")[0];

// Abrir modal
btn.onclick = function() {
	modal.style.display = "block";
}
// Cerrar modal
span.onclick = function() {
	modal.style.display = "none";
}
// Cerrar modal al dar click a cualquier cosa
window.onclick = function(event) {
	if (event.target == modal) {
		modal.style.display = "none";
	}
}

//Obtengo el correo guardado en llave "SessionTemporal" 
var CorreoShow =  localStorage.SessionTemporal;
//captura el tamaño del correo
var tam = CorreoShow.length;
var lista = document.getElementById("info");
//para mostrarselo recorte el caracter en la posicion 0 para que el numero respectivo no se muestre
//en el header de la pagina
lista.innerHTML = CorreoShow.substring(1, tam);

} //-------------- FIN FUNCION "INICIAR"

//------ Asociando función que manejará el evento load al cargar la página
if(window.addEventListener){
	window.addEventListener("load", iniciar, false);
}
else if(window.attachEvent){
	window.attachEvent("onload", iniciar);
}
