var imgsubidas = new Array();

function handleFileSelect(evt) {
  document.getElementById('iniciar').style.display = 'block';
  var files = evt.target.files; // FileList object
  // Recorre la FileList y renderiza los archivos de imagen como miniaturas.
  for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
      continue;
    }
    var reader = new FileReader();
    // Cierre para capturar la información del archivo.
    reader.onload = (function(theFile) {
      return function(e) {
        // Renderizar miniatura.
        var span = document.createElement('span');
        span.innerHTML = ['<img class="thumb" src="', e.target.result,
          '" title="', escape(theFile.name), '"/>'
        ].join('');

        document.getElementById('list').insertBefore(span, null);
        //Se agrega cada elemento al arreglo
        imgsubidas.push(e.target.result);



      };
    })(f);
    // Leer en el archivo de imagen como una URL de datos.
    reader.readAsDataURL(f);
  }
}
document.getElementById('files').addEventListener('change', handleFileSelect, false);
var array = localStorage.getItem('img');
// Se parsea para poder ser usado en js con JSON.parse
array = JSON.parse(array);

if (localStorage.img) {
  //verificamos cada elemento del arreglo para mostrar todas las imágenes
  for (var i = 0; i < array.length-1; i++) {
    var span = document.createElement('span');
    span.innerHTML += ['<img class="thumb" src="', array[i],
      '" title="test"/>'
    ].join('');
    document.getElementById('list').insertBefore(span, null);
  }
}

function iniciar() {
  var boton = document.getElementById("Enviar");
  if (boton.addEventListener) {
    boton.addEventListener("click", validar, false);
    
  } else if (boton.attachEvent) {
    boton.attachEvent("onclick", validar);
  }
}


function validar() {

  var modelo = document.getElementById("modelo").value;
  var descripcion = document.getElementById("descripcion").value;
  var precio = document.getElementById("precio").value;
  var datosldeauto = [modelo, descripcion, precio];

  imgsubidas.push(datosldeauto);
  localStorage.setItem('img', JSON.stringify(imgsubidas));

  var array2 = localStorage.getItem('img');
  array2 = JSON.parse(array2);
  swal({
    title: "Excelente!",
    text: "Auto Registrado",
    icon: "success",
    button: "Okay",
  });
  document.getElementById("mostrar").innerHTML = array2[array2.length-1];
  window.location = "../html/Mostrar.html";

}

//------ Asociando función que manejará el evento load al cargar la página
if (window.addEventListener) {
  window.addEventListener("load", iniciar, false);
} else if (window.attachEvent) {
  window.attachEvent("onload", iniciar);
}
