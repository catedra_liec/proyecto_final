function iniciar() {
	
	var CerrarSession = document.getElementById("CerrarSession");
	var Agregar = document.getElementById("Agregar");
	var Catalogo = document.getElementById("Catalogo");

	
	CerrarSession.onclick = function () {
		window.location = "../login.html";
	}
	Agregar.onclick = function(){
		window.location = "../html/Subir_Auto.html";
	}
	Catalogo.onclick = function(){
		window.location = "../html/Mostrar.html";
	}


	//con este for itero numeros de 0 a 100 que son los posibles numeros 
	//que el usario contenga en sus datos 
	for (var i = 0; i < 10; i++) {
		//capturo el correo almacenado en la llave "SeesionTemporal"
		var CorreoLS = localStorage.SessionTemporal;
		//obtengo el numero que esta en la posicion 0
		var j = CorreoLS.substring(0, 1);

		if (i == j) {
			//si el numero capturado en la posicion 0 coincide con algun numero del for
			//ESTO QUIERE DECIR QUE CALCULO LA POSICION DONDE TODOS LOS DATOS COINCIDEN  
			//CON EL NUMERO QUE TIENEN AL PRINCIPIO 
			//prosigo a obtener en ARREGLOS los datos de las llaves 

			var NombresUsuariosLS = [] = JSON.parse(localStorage.getItem("NombresUsuarios"));
			var ApellidosUsuariosLS = [] = JSON.parse(localStorage.getItem("ApellidosUsuarios"));
			var CorreosLS = [] = JSON.parse(localStorage.getItem("CorreosUsuarios"));
			var ContrasenasLS = [] = JSON.parse(localStorage.getItem("ContrasenasUsuarios"));
			var TipoUsuariosLS = [] = JSON.parse(localStorage.getItem("TipoUsuarios"));

			
			//captura el id de los elementos "a" donde se mostraran los datos
			var Nombre = document.getElementById("Nombre");
			var Apellido = document.getElementById("Apellido");
			var Correo = document.getElementById("Correo");
			var Contraseña = document.getElementById("Contraseña");
			var Rol = document.getElementById("Rol");

			//calculo el tamaño de todos los datos de este usuario
			var tamName = NombresUsuariosLS[i].length;
			var tamLastName = ApellidosUsuariosLS[i].length;
			var tamMail = CorreosLS[i].length;
			var tamPass = ContrasenasLS[i].length;

			//muestro en el elemento "a" la info recortando el numero que se encuentra al principio
			//con un substring que solo recortan el caracter en la posicion 0
			Nombre.innerHTML = NombresUsuariosLS[i].substring(1, tamName);
			Apellido.innerHTML = ApellidosUsuariosLS[i].substring(1, tamLastName);
			Correo.innerHTML = CorreosLS[i].substring(1, tamMail);
			Contraseña.innerHTML = ContrasenasLS[i].substring(1, tamPass);

			//con respecto al Rol, el numero 0 = Admin y 1 = Usuario, a estos datos no se les agrega
			//la variable iterada "i" del registro porque este si se puede repetir
			var Agregar = document.getElementById("Agregar");
			var Catalogo = document.getElementById("Catalogo");

			if(TipoUsuariosLS[i] == 0){
				Rol.innerHTML = "Administrador";
			}
			else if(TipoUsuariosLS[i] == 1){				
				Rol.innerHTML = "Usuario";
				Agregar.style.visibility = "hidden";
				Catalogo.style.visibility = "hidden";
			}

			

						
		}

	}

} //-------------- FIN FUNCION "INICIAR"


//------ Asociando función que manejará el evento load al cargar la página
if (window.addEventListener) {

	window.addEventListener("load", iniciar, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", iniciar);
}