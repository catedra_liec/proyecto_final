function d1(selectTag) {
	if (selectTag.value == '0') {
		document.getElementById('ClaveAdminn').disabled = false;
	} else {
		document.getElementById('ClaveAdminn').disabled = true;
	}
}

function soloLetras(e) {
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toLowerCase();
	letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
	especiales = "8-37-39-46";
	tecla_especial = false
	for (var i in especiales) {
		if (key == especiales[i]) {
			tecla_especial = true;
			break;
		}
	}
	if (letras.indexOf(tecla) == -1 && !tecla_especial) {
		return false;
	}
}

function iniciar() {

	var boton = document.getElementById("Enviar");
	if (boton.addEventListener) {
		boton.addEventListener("click", validar, false);
	} else if (boton.attachEvent) {
		boton.attachEvent("onclick", validar);
	}
}

function validar() {
	var nombre = document.frmdatos.nombres.value;
	var apellido = document.frmdatos.apellidos.value;
	var correo = document.frmdatos.correo.value;
	var contra = document.frmdatos.contra.value;
	var verificar = document.frmdatos.contradmin.value;
	var Tipos = document.getElementById("tipousuario").selectedIndex;
	var valido = false;
	var re = null;
	if (Tipos == '0') {
		if (nombre.length == 0 || apellido.length == 0 || correo.length == 0 || contra.length == 0 || verificar.length == 0) {

			swal({
				title: "Error",
				text: "Existen campos vacíos dentro del formulario",
				icon: "error",
				button: "Reintentar",
			});

			return 0;
		}
	}

	if (Tipos == '1') {
		if (nombre.length == 0 || apellido.length == 0 || correo.length == 0 || contra.length == 0) {

			swal({
				title: "Error",
				text: "Existen campos vacíos dentro del formulario",
				icon: "error",
				button: "Reintentar",
			});

			return 0;
		}
	}

	//------------ Validar formato de la contraseña
	re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
	if (re.test(contra)) {
		valido = true;

		//------------ Validar formato de correo electrónico
		re = /^[\w-.]+\@[\w-]+\.[a-z]{2,3}(\.[a-z]{2})?$/i;
		if (re.test(correo)) {
			valido = true;

			//FUNCION AQUI IBAS

			var Tipos = document.getElementById("tipousuario").selectedIndex;

			if (Tipos == "1") { //el 1 representa a los usuarios

				swal({
					title: "Excelente",
					text: "Usuario registrado",
					icon: "success",
					button: "okay",
				}, );

				//Funcion para crear e insertar las llaves y datos en el local storage
				InsertarDatosLocalStorage(Tipos);				
				window.location.href = "../login.html"


			} else {
				var VerificarClave = document.getElementById("tipoadminhidden").value; //input hidden
				var ClaveAdmin = document.getElementById("ClaveAdminn").value; //txt para escribir clave
				var bandera = false;

				if (Tipos == "0" && ClaveAdmin == VerificarClave) { //el 0 representa a los admins
					bandera = true;
					swal({
						title: "Excelente",
						text: "Administrador registrado",
						icon: "success",
						button: "okay",
					});

					//Funcion para crear e insertar las llaves y datos en el local storage
					InsertarDatosLocalStorage(Tipos);					
					window.location.href = "../login.html";
				}
				if (bandera != true) {
					swal({
						title: "Error",
						text: "Clave de administrador incorrecta",
						icon: "info",
						button: "Reintentar",
					});
				}
			}


		} else {

			swal({
				title: "Error",
				text: "Dirección de correo electrónico no válida",
				icon: "info",
				button: "Reintentar",
			});

		}

	} else {
		swal({
			title: "Error",
			text: "Formato de contraseña no válido",
			icon: "info",
			button: "Reintentar",
		});

	}


} //---------------- FIN FUNCTION VALIDAR



function InsertarDatosLocalStorage(Tipos) {
	//captura los datos que estan en la llave "NumerosUsuarios"
	var NumerosUsuarios = JSON.parse(localStorage.getItem("NumerosUsuarios")) || [];
	//en "i" guardo la cantidad de datos que hay en la llave anteriormente obtenida
	// asi esta variable aumentara cada vez que se registre alguien nuevo y se asignara a cada usuario respectivo
	i = NumerosUsuarios.length;

	//normal creo la llave para todos los datos con la variable de iteracion "i"
	var NombresUsuarios = JSON.parse(localStorage.getItem("NombresUsuarios")) || [];
	var Nombres = document.frmdatos.nombres.value;
	NombresUsuarios.push(i + Nombres);
	localStorage.setItem("NombresUsuarios", JSON.stringify(NombresUsuarios));

	var ApellidosUsuarios = JSON.parse(localStorage.getItem("ApellidosUsuarios")) || [];
	var Apellidos = document.frmdatos.apellidos.value;
	ApellidosUsuarios.push(i + Apellidos);
	localStorage.setItem("ApellidosUsuarios", JSON.stringify(ApellidosUsuarios));

	var CorreosUsuarios = JSON.parse(localStorage.getItem("CorreosUsuarios")) || [];
	var Correos = document.frmdatos.correo.value;
	CorreosUsuarios.push(i + Correos);
	localStorage.setItem("CorreosUsuarios", JSON.stringify(CorreosUsuarios));

	var ContrasenasUsuarios = JSON.parse(localStorage.getItem("ContrasenasUsuarios")) || [];
	var Cortrasenas = document.frmdatos.contra.value;
	ContrasenasUsuarios.push(i + Cortrasenas);
	localStorage.setItem("ContrasenasUsuarios", JSON.stringify(ContrasenasUsuarios));

	var TipoUsuarios = JSON.parse(localStorage.getItem("TipoUsuarios")) || [];
	TipoUsuarios.push(Tipos);
	localStorage.setItem("TipoUsuarios", JSON.stringify(TipoUsuarios));


	//Creo la llave "NumerosUsuarios" para crear identificador de usuarios
	var NumerosUsuarios = JSON.parse(localStorage.getItem("NumerosUsuarios")) || [];
	NumerosUsuarios.push(i);
	localStorage.setItem("NumerosUsuarios", JSON.stringify(NumerosUsuarios));
}


//------ Asociando función que manejará el evento load al cargar la página
if (window.addEventListener) {
	window.addEventListener("load", iniciar, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", iniciar);
}